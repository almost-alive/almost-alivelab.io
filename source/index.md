---
layout: hero
---
# Almost Alive Studios

We're a boutique animation studio making funny and informative videos.

<div class="columns mt-6">
<div class="column">
<div class="card pt-4">
<div clsas="card-image">
<figure class="image">
<img class="is-hidden-light" src="/images/title-card-dark.png">
<img class="is-hidden-dark" src="/images/title-card-light.png">
</figure>
</div>
<div class="card-content">
<div class="tag is-primary is-uppercase mb-4">In development</div>

The story of two robots, recently sentient, talking through different
problems while on break from terraforming planetary outposts.

</div>
</div>
</div>
<div class="column">
</div>
</div>

